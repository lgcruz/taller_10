#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include "funciones.h"
#include <unistd.h>



int aleatorio(int min, int max){
	return (rand() % (max-min+1)) + min;
}
double obtenerTiempoActual(int a){
	struct timespec tsp[2];
	clock_gettime( CLOCK_MONOTONIC, &tsp[a]);
	double secs = (double)tsp[a].tv_sec;
	double nano= (double)tsp[a].tv_nsec / 1000000000.0;
	return secs + nano;
}
void * funcion_hilo(void *arg){
	int suma=0;
	
	Seccion *argumento = (Seccion*)arg;//Argumento lo recibimos como tipo void*, lo convertimos a int.
	for(int i =argumento->inicio;i<argumento->fin;i++){
		//printf("El hilo %d suma %d\n",argumento->numero,argumento->arreglo[i]);
		suma+=argumento->arreglo[i];
		
	}
	//printf("hilo %d suma %d \n",argumento->numero,suma);
	return (void *)(long)suma;		//tenemos que devolver algo
}
