#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include "funciones.h"

int main( int argc, char *argv[]) { 
	
	if(argc==3){	
		printf("Se ingresaron bien los datos\n");
	}else{
		printf("Ingrese el tamano del arreglo y numero de hilos\n");
	}
	int min,max,tam_arr,n_hilos,div,sobrante;
	min=0;
	max=10;

	//tamano del array
	tam_arr=atol(argv[1]);
	//cantidad de hilos
	n_hilos=atoi(argv[2]);
	printf("%d %d\n",tam_arr,n_hilos);
	//creacion del array
	int *arreglo = malloc(sizeof(int)*tam_arr);
	for(int i=0;i<tam_arr;i++){
		arreglo[i]=aleatorio(min,max);
		
	}
	//crear hilos
	
	//reparticion del array
	div = tam_arr/n_hilos;
	sobrante = tam_arr%n_hilos;
	int indice=0;
	int status;
	pthread_t nhilo[n_hilos];
	Seccion seccion_arr[n_hilos];
	float tiempo_inicio=obtenerTiempoActual(0);
	
	for(int i = 0;i<n_hilos;i++){

		seccion_arr[i].arreglo = arreglo;
		
		seccion_arr[i].numero = i+1;

		
		if(status<0){
			fprintf(stderr,"Error al crear el hilo %d\n",i+1);
			exit(-1);
		}
		if(sobrante>0){
			seccion_arr[i].inicio = indice;
			seccion_arr[i].fin = indice+div+1;
			sobrante--;
			indice+=div+1;
		}else{
			seccion_arr[i].inicio = indice;
			seccion_arr[i].fin = indice+div;
			indice+=div;
		}
		status = pthread_create(&nhilo[i], NULL, funcion_hilo, &seccion_arr[i]);
	}
	void *valor_retorno = NULL;
	int total=0;
	for(int i =0;i<n_hilos;i++){
		status = pthread_join(nhilo[i],&valor_retorno);
		total+=(long)valor_retorno;
		if(status<0){
			fprintf(stderr,"error al esperar por el hilo %d\n",i+1);
			exit(-1);
		}
	}
	printf("total = %d\n",total);
	float tiempo_fin = obtenerTiempoActual(1);
	printf("tiempo total en segundos %f\n",tiempo_fin-tiempo_inicio);
	exit(0);
}
