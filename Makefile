hilos: main.o funciones.o
	gcc -g -pthread obj/main.o obj/funciones.o -o bin/hilos
main.o: src/main.c
	gcc -g -Wall -c -I include/ src/main.c -o obj/main.o
funciones.o: src/funciones.c include/funciones.h
	gcc -g -Wall -c -I include/ src/funciones.c -o obj/funciones.o
